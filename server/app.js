'use strict';

var spawn = Npm.require('child_process').spawn;

Meteor.methods({
  start: function() {
    var child = spawn('ls', ['/tmp']);
    var pid = child.pid;

    var wrappedChildStdoutOn = Meteor.wrapAsync(child.stdout.on, child.stdout);
    var wrappedChildStderrOn = Meteor.wrapAsync(child.stderr.on, child.stderr);
    var wrappedChildOn = Meteor.wrapAsync(child.on, child);

    // this insert() breaks upcoming insert() calls!
    Stuff.insert({pid: pid, date: new Date(), type: 'dummy', data: 'dummy'});

    wrappedChildStdoutOn('data',  function (data) {
      Stuff.insert({pid: pid, date: new Date(), type: 'stdout', data: data.toString()});
    });
   
    wrappedChildStderrOn('data', function (data) {
      Stuff.insert({pid: pid, date: new Date(), type: 'stderr', data: data.toString()});
    });
   
    wrappedChildOn('exit', function (code) {
      Stuff.insert({pid: pid, date: new Date(), type: 'exit', code: code});
    });
  }
});
