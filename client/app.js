Template.main.helpers({
  stuff: function() {
    return Stuff.find({}, {sort: {date: -1}})
  }
});

Template.main.events({
  'click button': function(event, template) {
    Meteor.call('start');
  }
});
